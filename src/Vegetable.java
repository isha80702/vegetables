import java.util.*;

// Abstract class Vegetable
abstract class Vegetable{
    // common features for each vegetable
    String color;
    double size;

	// Default constructor need to be declared
    Vegetable(){}
	// to get the size
    abstract double getsize();
    	// to get the color of vegetable
    abstract String getcolor();
    	// which type of vegetable it is
    abstract String getname();

	// prints that vegetable is rippen or not
    abstract void isRipe();

}