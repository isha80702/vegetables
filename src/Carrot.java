class Carrot extends Vegetable{

// initialization of data members of Carrot
    Carrot(String color,double size){
        this.color = color;
        this.size = size;
    }

    double getsize(){ return this.size;}
    String getname(){ return "Carrot";}
    String getcolor(){ return this.color;}


    void isRipe(){
        if(this.color == "orange" && this.size == 1.5) System.out.println("Rippen");
        else System.out.println("Not rippen");
    }

}