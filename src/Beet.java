class Beet extends Vegetable{

	// initialization of data members of Beet
    Beet(String color,double size){
        this.color = color;
        this.size = size;
    }

    double getsize(){ return this.size;}
    String getname(){ return "Beet";}
    String getcolor(){ return this.color;}

    void isRipe(){
        if(this.color == "red" && this.size == 2) System.out.println("Rippen");
        else System.out.println("Not rippen");
    }

}