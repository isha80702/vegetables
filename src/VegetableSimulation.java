class VegetableSimulation {
    public static void main(String args[]){
        // Arraylist is an array of objects
        ArrayList<Vegetable> list = new ArrayList<Vegetable>();
        // Here, type of object is Vegetable which can not instantiated 
        // so it has given reference of object of Beet or Carrot class
        Vegetable v1 = new Beet("red", 2);
        Vegetable v2 = new Beet("blue", 3);
        Vegetable b1 = new Carrot("orange", 1.5);
        Vegetable b2 = new Carrot("red", 2);
	// add all the object to arraylist
        list.add(v1);
        list.add(v2);
        list.add(b1);
        list.add(b2);
	
	// Display the information of each object
        for(int i=0;i<list.size();i++){
            Vegetable v = list.get(i);
            System.out.println(v.getname() + " "+ v.getcolor() + " "+ v.getsize());
            v.isRipe();
        }


    }    
}